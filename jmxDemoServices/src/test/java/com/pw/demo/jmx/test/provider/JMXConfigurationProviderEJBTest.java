package com.pw.demo.jmx.test.provider;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pw.demo.jmx.configuration.service.jmx.JMXConfigurationProvider;
import com.pw.demo.jmx.configuration.service.jmx.impl.util.MXBeanNameBuilder;
import com.pw.demo.jmx.configuration.service.jmx.impl.util.MXBeanUtils;
import com.pw.demo.jmx.exception.JMXDemoServiceException;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.common.IncorrectPaginationDataException;
import com.pw.demo.jmx.services.common.PaginationData;
import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMXBean;
import com.pw.demo.jmx.services.message.configuration.jmx.registration.MessageServiceJMXConfigurationDeploymentDescriptor;

@RunWith(Arquillian.class)
public class JMXConfigurationProviderEJBTest {

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap
				.create(JavaArchive.class)
				.addPackages(true, "com.pw.demo.jmx.configuration")
				.addPackages(true, "com.pw.demo.jmx.exception")
				.addPackages(true,
						"com.pw.demo.jmx.services.message.configuration")
				.addClass(MessageService.class).addClass(MessageType.class)
				.addClass(Message.class).addClass(PaginationData.class)
				.addClass(IncorrectPaginationDataException.class)
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private JMXConfigurationProvider jmxProvider;

	@Test
	public void testRegisterConfigurationIfMissing()
			throws JMXDemoServiceException {
		safeRegisterMessageServiceMBean();
		MessageServiceMXBean mBean = MXBeanUtils.getMXBean(
				MXBeanNameBuilder.createJMXBeanName(MessageService.class),
				MessageServiceMXBean.class);
		assertNotNull(mBean);
	}

	@Test(expected = JMXDemoServiceException.class)
	public void testRegisterConfigurationExistsError()
			throws JMXDemoServiceException {
		jmxProvider
				.registerConfiguration(new MessageServiceJMXConfigurationDeploymentDescriptor());
		jmxProvider
				.registerConfiguration(new MessageServiceJMXConfigurationDeploymentDescriptor());
	}

	@Test
	public void testGetConfiguration() throws JMXDemoServiceException {
		safeRegisterMessageServiceMBean();
		MessageServiceMXBean mbean = jmxProvider.getServiceMBean(
				MessageService.class, MessageServiceMXBean.class);
		assertNotNull(mbean);
	}

	@Test
	public void testSetConfigurationValues() throws JMXDemoServiceException {
		safeRegisterMessageServiceMBean();
		MessageServiceMXBean mbean = jmxProvider.getServiceMBean(
				MessageService.class, MessageServiceMXBean.class);
		assertNotNull(mbean);
		Set<MessageType> filter = mbean.showFilter();
		assertNotNull(filter);
		assertTrue(filter.isEmpty());
		mbean.filter(MessageType.IN);

		mbean = jmxProvider.getServiceMBean(MessageService.class,
				MessageServiceMXBean.class);
		filter = mbean.showFilter();
		assertFalse(filter.isEmpty());
		assertTrue(filter.contains(MessageType.IN));

	}

	private void safeRegisterMessageServiceMBean()
			throws JMXDemoServiceException {
		jmxProvider
				.registerConfigurationIfMissing(new MessageServiceJMXConfigurationDeploymentDescriptor());
	}
}
