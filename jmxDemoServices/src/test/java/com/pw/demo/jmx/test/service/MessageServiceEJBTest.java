package com.pw.demo.jmx.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.pw.demo.jmx.configuration.service.jmx.JMXConfigurationProvider;
import com.pw.demo.jmx.exception.JMXDemoServiceException;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.common.PaginationData;
import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMXBean;
import com.pw.demo.jmx.test.AbstractEntityTestPlatform;

@RunWith(Arquillian.class)
public class MessageServiceEJBTest extends AbstractEntityTestPlatform {

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap
				.create(JavaArchive.class)
				.addPackages(true, "com.pw.demo.jmx")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsResource("jbossas-ds.xml", "META-INF/jbossas-ds.xml")
				.addAsResource("test-persistence.xml",
						"META-INF/persistence.xml");
	}

	private static final String OUT_MESSAGE_TEXT = "This is OUT Message number ";
	private static final String IN_MESSAGE_TEXT = "This is IN Message number ";
	private static final int OUT_MESSAGE_COUNT = 10;
	private static final int IN_MESSAGE_COUNT = 20;
	private static final PaginationData DEFAULT_PAGINATION = new PaginationData(
			OUT_MESSAGE_COUNT + IN_MESSAGE_COUNT, 1);
	private static final Integer ITEMS_PER_PAGE = 5;

	@Inject
	private MessageService messageService;

	@Inject
	private JMXConfigurationProvider configurationService;

	@Before
	public void setUp() throws Exception {
		clearData();
		initializeData();
		forceServiceInitialization();
	}

	@After
	public void tearDown() {
		try {
			commitTransaction();
		} catch (Exception e) {
			// We need to try to commit because failed tests might not do
			// that.
		}
	}

	@Test
	public void testListAllMessages() throws Exception {
		beginTransaction();
		setEmptyFilterInServiceConfigurationMXBean();
		List<Message> messages = messageService
				.listMessages(DEFAULT_PAGINATION);
		assertEquals(OUT_MESSAGE_COUNT + IN_MESSAGE_COUNT, messages.size());
		assertTrue(messages.contains(createMessage(0, MessageType.IN)));
		assertTrue(messages.contains(createMessage(0, MessageType.OUT)));
		assertTrue(messages.contains(createMessage(IN_MESSAGE_COUNT - 1,
				MessageType.IN)));
		assertTrue(messages.contains(createMessage(OUT_MESSAGE_COUNT - 1,
				MessageType.OUT)));
		commitTransaction();
	}

	@Test
	public void testListAllMessagesPagination() throws Exception {
		beginTransaction();
		setEmptyFilterInServiceConfigurationMXBean();
		List<Message> messages = messageService
				.listMessages(new PaginationData(ITEMS_PER_PAGE, 1));
		assertNotNull(messages);
		assertEquals(ITEMS_PER_PAGE, (Integer) messages.size());
		assertEquals(createMessage(0, MessageType.IN), messages.get(0));
		messages = messageService.listMessages(new PaginationData(
				ITEMS_PER_PAGE, 5));
		assertNotNull(messages);
		assertEquals(ITEMS_PER_PAGE, (Integer) messages.size());
		assertEquals(createMessage(0, MessageType.OUT), messages.get(0));
		commitTransaction();
	}

	@Test
	public void testListFilteredMessages() throws Exception {
		beginTransaction();
		MessageServiceMXBean config = getServiceConfiguration();
		setFilteredMessageTypeAndVerifyCount(config, MessageType.IN,
				OUT_MESSAGE_COUNT);
		setFilteredMessageTypeAndVerifyCount(config, MessageType.OUT,
				IN_MESSAGE_COUNT);
		commitTransaction();
	}

	@Test
	public void testListFilteredMessagesPagination() throws Exception {
		beginTransaction();
		MessageServiceMXBean config = getServiceConfiguration();
		config.filter(MessageType.OUT);
		List<Message> messages = messageService
				.listMessages(new PaginationData(ITEMS_PER_PAGE, 1));
		assertNotNull(messages);
		assertEquals(ITEMS_PER_PAGE, (Integer) messages.size());
		assertEquals(createMessage(0, MessageType.IN), messages.get(0));
		messages = messageService.listMessages(new PaginationData(
				ITEMS_PER_PAGE, 2));
		assertNotNull(messages);
		assertEquals(ITEMS_PER_PAGE, (Integer) messages.size());
		assertEquals(createMessage(ITEMS_PER_PAGE, MessageType.IN),
				messages.get(0));
		commitTransaction();
	}

	private void clearData() throws Exception {
		beginAndJoinTransaction();
		getEntityManager().createQuery("delete from Message").executeUpdate();
		commitTransaction();
	}

	private Message createMessage(Integer number, MessageType type) {
		String formattedNumber = String.format("%08d", number);
		switch (type) {
		case IN:
			return new Message(IN_MESSAGE_TEXT + formattedNumber, type);
		case OUT:
			return new Message(OUT_MESSAGE_TEXT + formattedNumber, type);
		default:
			throw new IllegalArgumentException("Unsupported Message Type");
		}
	}

	private void forceServiceInitialization() throws JMXDemoServiceException {
		/*
		 * The EJB might not be loaded until it is first called, so it's
		 * configuration might not be available. Instead of loading the
		 * configuration manually we force loading of the ejb by calling an
		 * arbitrary method.
		 */
		@SuppressWarnings("unused")
		List<Message> messages = messageService
				.listMessages(DEFAULT_PAGINATION);

	}

	private MessageServiceMXBean getServiceConfiguration()
			throws JMXDemoServiceException {
		MessageServiceMXBean config = configurationService.getServiceMBean(
				MessageService.class, MessageServiceMXBean.class);
		return config;
	}

	private void initializeData() throws Exception {
		beginTransaction();
		for (int i = 0; i < OUT_MESSAGE_COUNT; i++) {
			persistOutMessage(i);
		}

		for (int i = 0; i < IN_MESSAGE_COUNT; i++) {
			persistInMessage(i);
		}
		commitTransaction();
	}

	private void persistInMessage(Integer number) {
		persistMessage(number, MessageType.IN);
	}

	private void persistMessage(Integer number, MessageType type) {
		getEntityManager().persist(createMessage(number, type));
	}

	private void persistOutMessage(Integer number) {
		persistMessage(number, MessageType.OUT);
	}

	private void setEmptyFilterInServiceConfigurationMXBean()
			throws JMXDemoServiceException {
		MessageServiceMXBean config = getServiceConfiguration();
		config.clear();
		;
	}

	private void setFilteredMessageTypeAndVerifyCount(
			MessageServiceMXBean config, MessageType type, int count)
			throws JMXDemoServiceException {
		config.clear();
		config.filter(type);
		List<Message> messages = messageService
				.listMessages(DEFAULT_PAGINATION);
		assertEquals(count, messages.size());
	}
}
