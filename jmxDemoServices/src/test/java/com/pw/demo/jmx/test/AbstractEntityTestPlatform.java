package com.pw.demo.jmx.test;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.pw.demo.jmx.JMXDemoConstants;

public class AbstractEntityTestPlatform {

	@PersistenceContext(unitName = JMXDemoConstants.PERSISTENCE_UNIT_NAME)
	private EntityManager em;
	@Inject
	private UserTransaction utx;

	public AbstractEntityTestPlatform() {
		super();
	}

	protected void beginAndJoinTransaction() throws NotSupportedException,
			SystemException {
		beginTransaction();
		joinTransaction();
	}

	protected void beginTransaction() throws NotSupportedException,
			SystemException {
		utx.begin();
	}

	protected void commitTransaction() throws RollbackException,
			HeuristicMixedException, HeuristicRollbackException,
			SystemException {
		utx.commit();
	}

	protected EntityManager getEntityManager() {
		return em;
	}

	private void joinTransaction() {
		getEntityManager().joinTransaction();
	}

}