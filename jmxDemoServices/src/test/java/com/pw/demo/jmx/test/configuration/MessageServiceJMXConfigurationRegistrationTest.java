package com.pw.demo.jmx.test.configuration;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.jmx.registration.MessageServiceJMXConfigurationDeploymentDescriptor;

public class MessageServiceJMXConfigurationRegistrationTest {

	private MessageServiceJMXConfigurationDeploymentDescriptor descriptor;

	@Before
	public void setUp() {
		descriptor = new MessageServiceJMXConfigurationDeploymentDescriptor();
	}

	@Test
	public void test() {
		assertEquals(MessageService.class.getSimpleName(), descriptor.getName());
		assertEquals(MessageService.class, descriptor.getServiceInterface());
	}

}
