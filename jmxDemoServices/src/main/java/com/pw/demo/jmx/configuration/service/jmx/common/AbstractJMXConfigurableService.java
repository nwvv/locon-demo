package com.pw.demo.jmx.configuration.service.jmx.common;

import javax.ejb.EJB;
import javax.management.MXBean;

import com.pw.demo.jmx.configuration.common.ConfigurationMapper;
import com.pw.demo.jmx.configuration.common.pojo.POJOStyleServiceConfiguration;
import com.pw.demo.jmx.configuration.common.technology.JMXBasedServiceConfiguration;
import com.pw.demo.jmx.configuration.service.common.AbstractService;
import com.pw.demo.jmx.configuration.service.jmx.JMXConfigurationProvider;
import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * Abstract base class for Services utilizing JMX Configuration mechanism.
 * Provides additional methods for obtaining Service's configuration from an
 * MXBean. While the MXBean holds the configuration and allows for its
 * modification from the outside, internally, the service should use a
 * technology-unaware POJO to obtain it's configuration, exposed via
 * getConfiguration().
 * 
 * @param <ServiceInterface>
 *            the main interface this service will be providing
 * @param <MXBeanInterface>
 *            interface of the {@link MXBean} holding the service's
 *            configuration
 * @param <ConfigurationPOJOType>
 *            class of the services generic configuration POJO
 * @author Przemyslaw Walat
 */
public abstract class AbstractJMXConfigurableService<ServiceInterface, MXBeanInterface extends JMXBasedServiceConfiguration<ServiceInterface>, ConfigurationPOJOType extends POJOStyleServiceConfiguration<ServiceInterface>>
		extends AbstractService<ServiceInterface> {

	private final Class<MXBeanInterface> beanInterface;

	@EJB
	private JMXConfigurationProvider jMXConfigurationService;

	/**
	 * Constructor
	 * 
	 * @param serviceInterface
	 *            - Interface the service exposes, this is used to manage the
	 *            service's configuration
	 * @param beanInterface
	 */
	public AbstractJMXConfigurableService(
			Class<ServiceInterface> serviceInterface,
			Class<MXBeanInterface> beanInterface) {
		super(serviceInterface);
		this.beanInterface = beanInterface;
	}

	/**
	 * Get the service configuration as a POJO.
	 * 
	 * @return An instance of service's configuration POJO
	 * @throws JMXDemoServiceException
	 */
	protected ConfigurationPOJOType getConfiguration()
			throws JMXDemoServiceException {
		MXBeanInterface mxBean = getMXBeanProxy();
		return getConfigurationMapper().map(mxBean);
	}

	/**
	 * Extending classes should provide implementation for this method. It
	 * should return an appropriate {@link ConfigurationMapper}, capable of
	 * translating an MXBean associated with the service to a POJO-style
	 * configuration
	 * 
	 * @return An instance {@link ConfigurationMapper} appropriate for the
	 *         service
	 */
	protected abstract ConfigurationMapper<ServiceInterface, MXBeanInterface, ConfigurationPOJOType> getConfigurationMapper();

	/**
	 * Get a reference to {@link JMXConfigurationProvider}
	 * 
	 * @return an instance of {@link JMXConfigurationProvider}
	 */
	protected JMXConfigurationProvider getjMXConfigurationService() {
		return jMXConfigurationService;
	}

	/**
	 * Returns a proxy to the service's MXBean. <b>Should not</b> be used to
	 * obtain configuration, in such case use getConfiguration() instead.
	 * 
	 * @return an instance of the service's MXBean
	 * @throws JMXDemoServiceException
	 */
	protected MXBeanInterface getMXBeanProxy() throws JMXDemoServiceException {
		MXBeanInterface mbean = getjMXConfigurationService().getServiceMBean(
				getServiceInterface(), getBeanInterface());
		return mbean;
	}

	private Class<MXBeanInterface> getBeanInterface() {
		return beanInterface;
	}

}