package com.pw.demo.jmx.persistence.dao.message.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.pw.demo.jmx.persistence.dao.common.AbstractDAO;
import com.pw.demo.jmx.persistence.dao.message.MessageDAO;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.persistence.model.message.query.MessageQueries;
import com.pw.demo.jmx.services.common.PaginationData;

/**
 * EJB implementation of MessageDAO
 * 
 * @author Przemysław Walat
 *
 */
@Stateless
public class MessageDAOEJB extends AbstractDAO<Message> implements MessageDAO {

	/**
	 * 
	 * Constructor for MessageDAOEJB
	 */
	public MessageDAOEJB() {
		super(Message.class);
	}

	@Override
	public List<Message> getAll(PaginationData paginationData, Boolean readOnly) {
		TypedQuery<Message> query = createTypedQueryForEntity(MessageQueries.GetAll.NAME);
		if (readOnly) {
			optimizeForReadOnlyAccess(query);
		}
		applyPaging(query, paginationData);
		return query.getResultList();
	}

	@Override
	public List<Message> getFiltered(PaginationData paginationData,
			List<MessageType> filter, Boolean readOnly) {
		TypedQuery<Message> query = createTypedQueryForEntity(MessageQueries.GetFiltered.NAME);
		query.setParameter(
				MessageQueries.GetFiltered.Parameters.FILTERED_MESSAGE_TYPES,
				filter);
		if (readOnly) {
			optimizeForReadOnlyAccess(query);
		}
		applyPaging(query, paginationData);
		return query.getResultList();
	}

}
