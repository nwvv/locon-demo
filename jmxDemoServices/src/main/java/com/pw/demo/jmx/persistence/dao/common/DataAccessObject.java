package com.pw.demo.jmx.persistence.dao.common;

/**
 * A Base DAO providing basic CRUD operations for a specific Entity
 * 
 * 
 * @param <EntityClass>
 *            class of the entity supported by this {@link DataAccessObject}
 * @author Przemyslaw Walat
 */
public interface DataAccessObject<EntityClass> {

	/**
	 * Delete the Entity.
	 * 
	 * @param entity
	 */
	void delete(EntityClass entity);

	/**
	 * Fetch the Entity
	 * 
	 * @param id
	 * @return
	 */
	EntityClass get(Long id);

	/**
	 * Persist the Entity
	 * 
	 * @param message
	 */
	void persist(EntityClass message);

	/**
	 * Merge the Entity
	 * 
	 * @param message
	 * @return
	 */
	EntityClass merge(EntityClass message);

	/**
	 * Get the entity reference instead of the whole Entity
	 * 
	 * @param id
	 * @return
	 */
	EntityClass getReference(Long id);

}