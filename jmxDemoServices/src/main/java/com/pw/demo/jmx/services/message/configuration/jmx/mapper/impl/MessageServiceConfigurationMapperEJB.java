package com.pw.demo.jmx.services.message.configuration.jmx.mapper.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.message.configuration.MessageServiceConfiguration;
import com.pw.demo.jmx.services.message.configuration.jmx.mapper.MessageServiceConfigurationMapper;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMXBean;

/**
 * EJB implementation of {@link MessageServiceConfiguration}
 * 
 * @author Przemyslaw Walat
 *
 */
@Stateless
public class MessageServiceConfigurationMapperEJB implements
		MessageServiceConfigurationMapper {

	@Override
	public MessageServiceConfiguration map(MessageServiceMXBean mbean) {
		MessageServiceConfiguration configuration = new MessageServiceConfiguration();
		List<MessageType> types = new ArrayList<>();
		types.addAll(mbean.showFilter());
		configuration.setFilteredMessageTypes(types);
		return configuration;
	}
}
