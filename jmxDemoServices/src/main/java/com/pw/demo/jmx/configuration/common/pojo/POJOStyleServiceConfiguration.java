package com.pw.demo.jmx.configuration.common.pojo;

/**
 * Marking interface to for POJO version of a service Configuration.
 * 
 * @param <ServiceInterface>
 *            main interface of the service this configuration belongs to
 * @author Przemyslaw Walat
 *
 */
public interface POJOStyleServiceConfiguration<ServiceInterface> {

}
