package com.pw.demo.jmx.configuration.service.jmx.impl;

import javax.ejb.Stateless;

import com.pw.demo.jmx.configuration.deployment.jmx.JMXConfigurationDeploymentDescriptor;
import com.pw.demo.jmx.configuration.service.jmx.JMXConfigurationProvider;
import com.pw.demo.jmx.configuration.service.jmx.impl.util.MXBeanNameBuilder;
import com.pw.demo.jmx.configuration.service.jmx.impl.util.MXBeanUtils;
import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * EJB implementation of {@link JMXConfigurationProvider}
 * 
 * @author Przemyslaw Walat
 *
 */
@Stateless
public class JMXConfigurationProviderEJB implements JMXConfigurationProvider {

	@Override
	public <MXBeanInterface> MXBeanInterface getMXBean(
			String configurationName, Class<MXBeanInterface> mxBeanInterface)
			throws JMXDemoServiceException {
		String beanName = MXBeanNameBuilder
				.createJMXBeanName(configurationName);
		return MXBeanUtils.getMXBean(beanName, mxBeanInterface);
	}

	@Override
	public <ServiceInterface, MXBeanInterface> MXBeanInterface getServiceMBean(
			Class<ServiceInterface> serviceInterface,
			Class<MXBeanInterface> mxBeanInterface)
			throws JMXDemoServiceException {
		String beanName = MXBeanNameBuilder.createJMXBeanName(serviceInterface);
		return MXBeanUtils.getMXBean(beanName, mxBeanInterface);
	}

	@Override
	public boolean isRegistered(
			JMXConfigurationDeploymentDescriptor<?> descriptor)
			throws JMXDemoServiceException {
		return MXBeanUtils.isRegistered(MXBeanNameBuilder
				.createJMXBeanName(descriptor.getName()));
	}

	@Override
	public <MbeanInterface> void registerConfiguration(
			JMXConfigurationDeploymentDescriptor<MbeanInterface> descriptor)
			throws JMXDemoServiceException {
		String beanName = MXBeanNameBuilder.createJMXBeanName(descriptor
				.getName());
		MbeanInterface mBean = descriptor.createMXBean();
		MXBeanUtils.registerMXBean(beanName, mBean);
	}

	@Override
	public <MXBeanInterface> void registerConfigurationIfMissing(
			JMXConfigurationDeploymentDescriptor<MXBeanInterface> descriptor)
			throws JMXDemoServiceException {
		if (!isRegistered(descriptor)) {
			registerConfiguration(descriptor);
		}
	}

}
