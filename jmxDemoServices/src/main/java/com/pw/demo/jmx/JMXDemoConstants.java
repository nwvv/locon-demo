package com.pw.demo.jmx;

/**
 * Constants of JMX Demo Application
 * 
 * @author Przemyslaw Walat
 *
 */
public interface JMXDemoConstants {

	String PERSISTENCE_UNIT_NAME = "jmx-demo";
}
