package com.pw.demo.jmx.configuration.deployment.jmx;

import javax.management.MXBean;

import com.pw.demo.jmx.configuration.deployment.AbstractConfigurationDeploymentDescriptor;
import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * Abstract base Deployment Descriptor for configurations stored as
 * {@link MXBean} instances. Provides the interface of the MXBean holding the
 * configuration.
 * 
 * @param <MXBeanInterface>
 *            the interface of the MXBean used to store the configuration being
 *            registered
 * 
 * @author Przemyslaw Walat
 */
public abstract class JMXConfigurationDeploymentDescriptor<MXBeanInterface>
		extends AbstractConfigurationDeploymentDescriptor {

	private final Class<MXBeanInterface> mxBeanInterface;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            the name of the configuration to register
	 * @param mxBeanInterface
	 *            the interface of the MXBean used to store the configuration
	 */
	public JMXConfigurationDeploymentDescriptor(String name,
			Class<MXBeanInterface> mxBeanInterface) {
		super(name);
		this.mxBeanInterface = mxBeanInterface;
	}

	/**
	 * Creates an instance of the Configuration's MXBean. This method will be
	 * used to initialize the configuration.
	 * 
	 * @return An instance of the {@link MXBean} to hold the configuration
	 * @throws JMXDemoServiceException
	 */
	public abstract MXBeanInterface createMXBean()
			throws JMXDemoServiceException;

	/**
	 * Get the interface of the {@link MXBean} associated with this
	 * configuration.
	 * 
	 * @return The interface of the {@link MXBean}
	 */
	public Class<?> getMxBeanInterface() {
		return mxBeanInterface;
	}

}
