package com.pw.demo.jmx.services.message.configuration;

import java.util.List;

import com.pw.demo.jmx.configuration.common.pojo.AbstractServiceConfigurationPOJO;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.message.MessageService;

/**
 * Configuration POJO for {@link MessageService}
 * 
 * @author Przemyslaw Walat
 *
 */
public class MessageServiceConfiguration extends
		AbstractServiceConfigurationPOJO<MessageService> {

	private List<MessageType> filteredMessageTypes;

	/**
	 * 
	 * Constructor for MessageServiceConfiguration
	 */
	public MessageServiceConfiguration() {
		super(MessageService.class);
	}

	/**
	 * Get Filtered {@link MessageType}s
	 * 
	 */
	public List<MessageType> getFilteredMessageTypes() {
		return filteredMessageTypes;
	}

	/**
	 * Set filtered {@link MessageType}s
	 * 
	 * @param filteredMessageTypes
	 */
	public void setFilteredMessageTypes(List<MessageType> filteredMessageTypes) {
		this.filteredMessageTypes = filteredMessageTypes;
	}

}
