package com.pw.demo.jmx.configuration.service.jmx.impl.util;

import java.lang.management.ManagementFactory;

import javax.management.InstanceAlreadyExistsException;
import javax.management.JMX;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MXBean;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * Utility class containing {@link MXBean}-related operations.
 * 
 * @author Przemyslaw Walat
 *
 */
public class MXBeanUtils {

	/**
	 * Check whether there is an {@link MXBean} registered under the specified
	 * name
	 * 
	 * @param beanName
	 *            Name of the bean to look for
	 * @return true if an {@link MXBean} is registered under the name given,
	 *         false otherwise
	 * @throws JMXDemoServiceException
	 */
	public static boolean isRegistered(String beanName)
			throws JMXDemoServiceException {
		MBeanServer server = getMBeanServer();
		ObjectName name = createObjectName(beanName);
		return server.isRegistered(name);
	}

	/**
	 * Get a proxy for an {@link MXBean}
	 * 
	 * @param beanName
	 *            the name of the bean to get proxy for
	 * @param beanInterface
	 *            the interface of the bean to get proxy for
	 * @return the proxy
	 * @throws JMXDemoServiceException
	 */
	public static <MXBeanInterface> MXBeanInterface getMXBean(String beanName,
			Class<MXBeanInterface> beanInterface)
			throws JMXDemoServiceException {
		MBeanServer mBeanServer = getMBeanServer();
		ObjectName objectName = createObjectName(beanName);
		MXBeanInterface mBean = JMX.newMXBeanProxy(mBeanServer, objectName,
				beanInterface);
		return mBean;

	}

	/**
	 * Register an {@link MXBean}
	 * 
	 * @param beanName
	 *            name of the bean to register
	 * @param mBean
	 *            {@link MXBean} instance to register
	 * @throws JMXDemoServiceException
	 */
	public static void registerMXBean(String beanName, Object mBean)
			throws JMXDemoServiceException {
		try {
			MBeanServer mbeanServer = getMBeanServer();
			ObjectName objectName = createObjectName(beanName);
			mbeanServer.registerMBean(mBean, objectName);
		} catch (InstanceAlreadyExistsException | MBeanRegistrationException
				| NotCompliantMBeanException e) {
			throw new JMXDemoServiceException(
					"Exception during MBean registration", e);
		}
	}

	private static ObjectName createObjectName(String beanName)
			throws JMXDemoServiceException {
		try {
			ObjectName objectName = new ObjectName(beanName);
			return objectName;
		} catch (MalformedObjectNameException e) {
			throw new JMXDemoServiceException(
					"Exception occured when creating object name for an MBean",
					e);
		}

	}

	private static MBeanServer getMBeanServer() {
		return ManagementFactory.getPlatformMBeanServer();
	}
}
