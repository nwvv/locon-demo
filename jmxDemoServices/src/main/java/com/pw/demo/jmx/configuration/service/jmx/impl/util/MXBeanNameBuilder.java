package com.pw.demo.jmx.configuration.service.jmx.impl.util;

import javax.management.MXBean;

import com.pw.demo.jmx.configuration.deployment.jmx.JMXServiceConfigurationDeploymentDescriptor;

/**
 * Utility class for constructing {@link MXBean} names. Applies
 * application-specific prefix to names supplied to its methods.
 * 
 * @author Przemyslaw Walat
 *
 */
public class MXBeanNameBuilder {

	private static final String CONFIGURATION_SEPARATOR = ":service=";
	public static final String CONFIGURATION_DOMAIN = "com.pw.demo.jmx";
	public static final String CONFIGURATION_PREFIX = CONFIGURATION_DOMAIN
			+ CONFIGURATION_SEPARATOR;

	/**
	 * Create a fully qualified JMX bean name based on service interface class
	 * 
	 * @param serviceInterface
	 *            - interface of the service to build MBean name for
	 * @return
	 */
	public static String createJMXBeanName(Class<?> serviceInterface) {
		return createJMXBeanName(serviceInterface.getSimpleName());
	}

	/**
	 * Create a fully qualified JMX bean name based on service interface class
	 * 
	 * @param serviceInterface
	 *            - interface of the service to build MBean name for
	 * @return
	 */
	public static String createJMXBeanName(
			JMXServiceConfigurationDeploymentDescriptor<?, ?> configuration) {
		return createJMXBeanName(configuration.getName());
	}

	/**
	 * Create a fully qualified JMX bean name based on configuration name
	 * 
	 * @param configurationName
	 *            - name of the configuration to build MBean name for
	 * @return
	 */
	public static String createJMXBeanName(String configurationName) {
		return CONFIGURATION_PREFIX + configurationName;
	}

}
