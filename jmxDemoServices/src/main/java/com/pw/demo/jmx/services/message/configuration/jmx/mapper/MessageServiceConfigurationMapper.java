package com.pw.demo.jmx.services.message.configuration.jmx.mapper;

import javax.management.MXBean;

import com.pw.demo.jmx.configuration.common.ConfigurationMapper;
import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.MessageServiceConfiguration;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMXBean;

/**
 * The {@link ConfigurationMapper} for Message Service's {@link MXBean}-based
 * configuration.
 * 
 * @author Przemyslaw Walat
 *
 */
public interface MessageServiceConfigurationMapper
		extends
		ConfigurationMapper<MessageService, MessageServiceMXBean, MessageServiceConfiguration> {

	@Override
	MessageServiceConfiguration map(MessageServiceMXBean mbean);

}
