package com.pw.demo.jmx.services.message.configuration.jmx.mxbean;

import java.util.Set;

import javax.management.MXBean;

import com.pw.demo.jmx.configuration.common.technology.JMXBasedServiceConfiguration;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.MessageServiceConfiguration;

/**
 * JMX Representation of {@link MessageServiceConfiguration}
 * 
 * @author Przemyslaw Walat
 *
 */
@MXBean
public interface MessageServiceMXBean extends
		JMXBasedServiceConfiguration<MessageService> {
	/**
	 * Clears the filter
	 */
	void clear();

	/**
	 * Add a {@link MessageType} to the list of filtered types.
	 * 
	 * @param filteredType
	 *            {@link MessageType}s to add to the filter
	 */
	void filter(MessageType filteredType);

	/**
	 * Get filtered {@link MessageType}s
	 * 
	 * @return a Set of filtered {@link MessageType}s
	 */
	Set<MessageType> showFilter();
}
