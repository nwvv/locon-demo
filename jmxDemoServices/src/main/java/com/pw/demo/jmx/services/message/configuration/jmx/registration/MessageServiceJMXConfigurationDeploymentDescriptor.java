package com.pw.demo.jmx.services.message.configuration.jmx.registration;

import com.pw.demo.jmx.configuration.deployment.jmx.JMXServiceConfigurationDeploymentDescriptor;
import com.pw.demo.jmx.exception.JMXDemoServiceException;
import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMBeanImpl;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMXBean;

/**
 * Deployment Descriptor of {@link MessageService}'s JMX Configuration.
 * 
 * @author Przemyslaw Walat
 *
 */
public class MessageServiceJMXConfigurationDeploymentDescriptor
		extends
		JMXServiceConfigurationDeploymentDescriptor<MessageServiceMXBean, MessageService> {

	/**
	 * 
	 * Constructor for MessageServiceJMXConfigurationDeploymentDescriptor
	 */
	public MessageServiceJMXConfigurationDeploymentDescriptor() {
		super(MessageServiceMXBean.class, MessageService.class);
	}

	@Override
	public MessageServiceMXBean createMXBean() throws JMXDemoServiceException {
		return new MessageServiceMBeanImpl();
	}

}
