package com.pw.demo.jmx.services.message.configuration.jmx.mxbean;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.pw.demo.jmx.persistence.model.message.entity.MessageType;

/**
 * Implementation of {@link MessageServiceMXBean}
 * 
 * @author Przemyslaw Walat
 *
 */
public class MessageServiceMBeanImpl implements MessageServiceMXBean {

	private Set<MessageType> filteredMessageTypes = new HashSet<MessageType>();

	@Override
	public void clear() {
		filteredMessageTypes.clear();
	}

	@Override
	public void filter(MessageType filteredType) {
		filteredMessageTypes.add(filteredType);
	}

	@Override
	public Set<MessageType> showFilter() {
		return Collections.unmodifiableSet(filteredMessageTypes);
	}

}
