package com.pw.demo.jmx.services.message;

import java.util.List;

import com.pw.demo.jmx.exception.JMXDemoServiceException;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.services.common.PaginationData;

/**
 * The service allows for listing messages based on filter set in this service's
 * configuration.
 * 
 * @author Przemyslaw Walat
 */
public interface MessageService {

	/**
	 * Get a list of all {@link Message}s filtered according to the service
	 * configuration.
	 * 
	 * @return A List containing all Messages matching the filter configuration.
	 *         These are regular entities and are checked for changes during
	 *         transaction commit, so any changes to those objects will be
	 *         reflected in the database.
	 * @throws JMXDemoServiceException
	 */
	List<Message> listMessages(PaginationData pagingData)
			throws JMXDemoServiceException;

	/**
	 * Get a list of all {@link Message}s filtered according to the service
	 * configuration. The returned Messages are read-only meaning that any
	 * changes made to them will not be reflected in the DB.
	 * 
	 * @return A List containing all Messages matching the filter configuration.
	 *         These are regular entities and are <b>not</b> checked for changes
	 *         during transaction commit, so any changes to those objects
	 *         <b>won't</b> be reflected in the database.
	 * @throws JMXDemoServiceException
	 */
	List<Message> listMessagesReadOnly(PaginationData pagingData)
			throws JMXDemoServiceException;

}