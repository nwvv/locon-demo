package com.pw.demo.jmx.configuration.common.technology;

import javax.management.MXBean;

/**
 * Marking interface to for a service Configuration stored as an MXBean. This
 * version is exposed externally, and should not be used by application logic
 * internally, as it is coupled with {@link MXBean} code.
 * 
 * @param <ServiceInterface>
 *            main interface of the service this configuration belongs to
 * @author Przemyslaw Walat
 *
 */
public interface JMXBasedServiceConfiguration<ServiceInterface> extends
		TechnologySpecificServiceConfiguration<ServiceInterface> {

}
