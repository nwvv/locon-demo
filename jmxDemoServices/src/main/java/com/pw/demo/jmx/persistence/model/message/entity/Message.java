package com.pw.demo.jmx.persistence.model.message.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

import com.pw.demo.jmx.persistence.model.message.query.MessageQueries;

/**
 * Message Entity class.
 * 
 * @author Przemyslaw Walat
 *
 */
@NamedQueries({
		@NamedQuery(name = MessageQueries.GetFiltered.NAME, query = MessageQueries.GetFiltered.QUERY),
		@NamedQuery(name = MessageQueries.GetAll.NAME, query = MessageQueries.GetAll.QUERY) })
@Entity
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "message_id_generator")
	@SequenceGenerator(name = "message_id_generator", sequenceName = "MESSAGE_ENTITY_SEQ")
	private Long id;

	private String text;

	@Enumerated(EnumType.STRING)
	private MessageType type;

	/**
	 * 
	 * Default Constructor for Message
	 */
	public Message() {
	}

	/**
	 * 
	 * Constructor for Message
	 * 
	 * @param text
	 * @param type
	 */
	public Message(String text, MessageType type) {
		this.text = text;
		this.type = type;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	/**
	 * Get the {@link Message}'s text
	 * 
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Get the {@link Message}'s type
	 * 
	 * @return
	 */
	public MessageType getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/**
	 * Set the {@link Message}'s text
	 * 
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Get the {@link Message}'s type
	 * 
	 */
	public void setType(MessageType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return type + ": " + text;
	}

}
