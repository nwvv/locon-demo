package com.pw.demo.jmx.configuration.deployment.exception;

import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * An exception thrown when we try to fetch a configuration that has not been
 * registered.
 * 
 * @author Przemyslaw Walat
 *
 */
public class ConfigurationNotRegisteredException extends
		JMXDemoServiceException {

	private static final long serialVersionUID = -3350766845043285379L;

	public ConfigurationNotRegisteredException(String configurationName) {
		super("Configuration with name " + configurationName
				+ " is not registered");
	}

}
