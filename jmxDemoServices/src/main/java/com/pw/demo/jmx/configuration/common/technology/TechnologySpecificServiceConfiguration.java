package com.pw.demo.jmx.configuration.common.technology;

/**
 * Marking interface to for Technology Specific version of a service
 * Configuration. This version is exposed externally, and should not be used by
 * application logic internally, as it is typically coupled with a specific
 * technology.
 * 
 * @param <ServiceInterface>
 *            main interface of the service this configuration belongs to
 * @author Przemyslaw Walat
 *
 */
public interface TechnologySpecificServiceConfiguration<ServiceInterface> {

}
