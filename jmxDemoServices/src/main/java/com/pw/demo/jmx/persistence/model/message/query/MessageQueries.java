package com.pw.demo.jmx.persistence.model.message.query;

import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;

/**
 * Contains JPQL Queries related to {@link Message} entity.
 * 
 * @author Przemyslaw Walat
 *
 */
public interface MessageQueries {

	String ENTITY_NAME = "MESSAGE";

	/**
	 * Fetch all {@link Message} entities
	 *
	 */
	interface GetAll {
		String NAME = ENTITY_NAME + ".getAll";
		String QUERY = "SELECT m FROM Message m ORDER BY m.text,m.type";
	}

	/**
	 * Fetch all {@link Message} entities that have {@link MessageType} other
	 * than those supplied in the FILTERED_MESSAGE_TYPES argument.
	 *
	 */
	interface GetFiltered {
		interface Parameters {
			String FILTERED_MESSAGE_TYPES = "filtered";
		}

		String NAME = ENTITY_NAME + ".getFiltered";
		String QUERY = "SELECT m FROM Message m WHERE m.type not in :"
				+ Parameters.FILTERED_MESSAGE_TYPES + " ORDER BY m.text,m.type";

	}

}
