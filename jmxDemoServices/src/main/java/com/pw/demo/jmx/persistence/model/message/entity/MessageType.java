package com.pw.demo.jmx.persistence.model.message.entity;

import java.io.Serializable;

/**
 * MessageType enumeration.
 * 
 * @author Przemyslaw Walat
 *
 */
public enum MessageType implements Serializable {
	IN, OUT
}
