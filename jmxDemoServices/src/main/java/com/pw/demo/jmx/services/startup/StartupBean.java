package com.pw.demo.jmx.services.startup;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.pw.demo.jmx.configuration.service.jmx.JMXConfigurationProvider;
import com.pw.demo.jmx.exception.JMXDemoServiceException;
import com.pw.demo.jmx.persistence.dao.message.MessageDAO;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.common.PaginationData;
import com.pw.demo.jmx.services.message.MessageService;

@Singleton
@Startup
public class StartupBean {

	private static Logger LOG = Logger.getLogger(StartupBean.class.getName());

	@EJB
	private MessageService service;

	@EJB
	private MessageDAO messageDao;

	@EJB
	private JMXConfigurationProvider configProvider;

	@PostConstruct
	public void init() throws JMXDemoServiceException {
		LOG.info("Startup Bean running");
		addMessages();
		printMessages();
	}

	private void addMessages() {
		for (int i = 0; i < 5; i++) {
			messageDao.persist(createInMessage(i));
		}

		for (int i = 0; i < 5; i++) {
			messageDao.persist(createOutMessage(i));
		}
	}

	private Message createInMessage(int i) {
		return new Message("This is IN message " + i, MessageType.IN);
	}

	private Message createOutMessage(int i) {
		return new Message("This is OUT message " + i, MessageType.OUT);
	}

	@Schedule(minute = "*", hour = "*", second = "*/10")
	private void printMessages() throws JMXDemoServiceException {
		StringBuilder sb = new StringBuilder(
				"\nListing messages according to the filter\n");
		List<Message> messages = service
				.listMessages(new PaginationData(10, 1));
		for (Message message : messages) {
			sb.append("\t").append(message.toString()).append("\n");
		}
		LOG.info(sb.toString());
	}
}
