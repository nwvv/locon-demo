package com.pw.demo.jmx.configuration.common.pojo;

/**
 * Abstract base class for POJO-style {@link POJOStyleServiceConfiguration}s
 * 
 * @author Przemyslaw Walat
 */
public class AbstractServiceConfigurationPOJO<ServiceInterface> implements
		POJOStyleServiceConfiguration<ServiceInterface> {

	private final Class<ServiceInterface> serviceInterface;

	/**
	 * Constructor for AbstractServiceConfiguration
	 * 
	 * @param serviceInterface
	 *            main interface provided by the service this configuration
	 *            belongs to
	 */
	public AbstractServiceConfigurationPOJO(Class<ServiceInterface> serviceInterface) {
		this.serviceInterface = serviceInterface;
	}

	public Class<ServiceInterface> getServiceInterface() {
		return serviceInterface;
	}

}