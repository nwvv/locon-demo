package com.pw.demo.jmx.configuration.deployment.exception;

import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * An exception thrown when we try to register a configuration that already
 * exists.
 * 
 * @author Przemyslaw Walat
 *
 */
public class ConfigurationAlreadyRegisteredException extends
		JMXDemoServiceException {

	private static final long serialVersionUID = -3350766845043285379L;

	public ConfigurationAlreadyRegisteredException(String configurationName) {
		super("Configuration with name " + configurationName
				+ " is already registered");
	}

}
