package com.pw.demo.jmx.configuration.service.jmx;

import javax.management.MXBean;

import com.pw.demo.jmx.configuration.deployment.exception.ConfigurationAlreadyRegisteredException;
import com.pw.demo.jmx.configuration.deployment.jmx.JMXConfigurationDeploymentDescriptor;
import com.pw.demo.jmx.exception.JMXDemoServiceException;

/**
 * Provides methods dealing with management of {@link MXBean}-based
 * configurations.
 * 
 * @author Przemyslaw Walat
 *
 */
public interface JMXConfigurationProvider {

	/**
	 * Get an MXBean instance containing configuration for the given service
	 * 
	 * @param configurationName
	 *            name of the configuration to get an MXBean for
	 * @param mxBeanInterface
	 *            - interface of the MXBean
	 * @return
	 * @throws JMXDemoServiceException
	 */
	<ServiceInterface, MXBeanInterface> MXBeanInterface getServiceMBean(
			Class<ServiceInterface> serviceInterface,
			Class<MXBeanInterface> mxBeanInterface)
			throws JMXDemoServiceException;

	/**
	 * Get an MXBean instance for the given configuration
	 * 
	 * @param configurationName
	 *            name of the configuration to get an MXBean for
	 * @param mxBeanInterface
	 *            - interface of the MXBean
	 * @return
	 * @throws JMXDemoServiceException
	 */
	<MXBeanInterface> MXBeanInterface getMXBean(String configurationName,
			Class<MXBeanInterface> mxBeanInterface)
			throws JMXDemoServiceException;

	/**
	 * Tells whether a configuration specified by the provided descriptor is
	 * already registered.
	 * 
	 * @param descriptor
	 *            {@link JMXConfigurationDeploymentDescriptor} to check
	 * @return true if the configuration is already registered, false otherwise
	 * @throws JMXDemoServiceException
	 */
	boolean isRegistered(JMXConfigurationDeploymentDescriptor<?> descriptor)
			throws JMXDemoServiceException;

	/**
	 * Register a new JMX configuration.
	 * 
	 * @param descriptor
	 *            - {@link JMXConfigurationDeploymentDescriptor} to register
	 * @throws ConfigurationAlreadyRegisteredException
	 *             - when there already exists such a registration
	 * @throws JMXDemoServiceException
	 *             when other errors occur
	 */
	<MXBeanInterface> void registerConfiguration(
			JMXConfigurationDeploymentDescriptor<MXBeanInterface> descriptor)
			throws JMXDemoServiceException,
			ConfigurationAlreadyRegisteredException;

	/**
	 * Register a new JMX configuration, specified by the given descriptor. Will
	 * not register if such a registration already exists.
	 * 
	 * @param descriptor
	 *            - {@link JMXConfigurationDeploymentDescriptor} to register
	 * @throws JMXDemoServiceException
	 *             when other errors occur
	 */
	<MXBeanInterface> void registerConfigurationIfMissing(
			JMXConfigurationDeploymentDescriptor<MXBeanInterface> descriptor)
			throws JMXDemoServiceException;

}