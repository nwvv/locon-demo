package com.pw.demo.jmx.services.message.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.pw.demo.jmx.configuration.service.jmx.common.AbstractJMXConfigurableService;
import com.pw.demo.jmx.exception.JMXDemoServiceException;
import com.pw.demo.jmx.persistence.dao.message.MessageDAO;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.common.PaginationData;
import com.pw.demo.jmx.services.message.MessageService;
import com.pw.demo.jmx.services.message.configuration.MessageServiceConfiguration;
import com.pw.demo.jmx.services.message.configuration.jmx.mapper.MessageServiceConfigurationMapper;
import com.pw.demo.jmx.services.message.configuration.jmx.mxbean.MessageServiceMXBean;
import com.pw.demo.jmx.services.message.configuration.jmx.registration.MessageServiceJMXConfigurationDeploymentDescriptor;

/**
 * EJB implementation of {@link MessageService}
 * 
 * @author Przemyslaw Walat
 *
 */
@Stateless
public class MessageServiceEJB
		extends
		AbstractJMXConfigurableService<MessageService, MessageServiceMXBean, MessageServiceConfiguration>
		implements MessageService {

	@EJB
	private MessageServiceConfigurationMapper configurationMapper;

	@EJB
	private MessageDAO dao;

	public MessageServiceEJB() {
		super(MessageService.class, MessageServiceMXBean.class);
	}

	@Override
	public List<Message> listMessages(PaginationData pagingData)
			throws JMXDemoServiceException {
		return listMessages(pagingData, false);
	}

	@Override
	public List<Message> listMessagesReadOnly(PaginationData pagingData)
			throws JMXDemoServiceException {
		return listMessages(pagingData, true);
	}

	@Override
	protected MessageServiceConfigurationMapper getConfigurationMapper() {
		return configurationMapper;
	}

	private MessageDAO getDao() {
		return dao;
	}

	private List<MessageType> getFilteredMessageTypes()
			throws JMXDemoServiceException {
		MessageServiceConfiguration configuration = getConfiguration();
		return configuration.getFilteredMessageTypes();
	}

	@PostConstruct
	private void init() throws JMXDemoServiceException {
		initalizeConfiguration();
	}

	private void initalizeConfiguration() throws JMXDemoServiceException {
		getjMXConfigurationService().registerConfigurationIfMissing(
				new MessageServiceJMXConfigurationDeploymentDescriptor());
	}

	private List<Message> listMessages(PaginationData pagingData,
			Boolean readOnly) throws JMXDemoServiceException {
		List<MessageType> filter = getFilteredMessageTypes();
		if (filter.isEmpty()) {
			return getDao().getAll(pagingData, readOnly);
		} else {
			return getDao().getFiltered(pagingData, filter, readOnly);
		}
	}

}
