package com.pw.demo.jmx.configuration.common;

import com.pw.demo.jmx.configuration.common.pojo.POJOStyleServiceConfiguration;
import com.pw.demo.jmx.configuration.common.technology.TechnologySpecificServiceConfiguration;

/**
 * Maps from a technology-specific Configuration to a POJO-styled representation
 * 
 * 
 * @param <SourceType>
 *            type of
 * @author Przemyslaw Walat
 *
 */
public interface ConfigurationMapper<ServiceInterface, SourceType extends TechnologySpecificServiceConfiguration<ServiceInterface>, DestinationType extends POJOStyleServiceConfiguration<ServiceInterface>> {

	/**
	 * Map provided technology-specific configuration to a POJO-styled
	 * representation
	 * 
	 * @param source
	 * @return
	 */
	DestinationType map(SourceType source);
}
