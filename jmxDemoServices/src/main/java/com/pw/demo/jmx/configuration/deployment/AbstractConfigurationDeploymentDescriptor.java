package com.pw.demo.jmx.configuration.deployment;

/**
 * Abstract base class for Configuration Deployment Descriptor. Holds only the
 * name of the Configuration to deploy. Extending classes should provide further
 * details, specifying how a specific configuration should be deployed.
 * 
 * @author Przemyslaw Walat
 */
public abstract class AbstractConfigurationDeploymentDescriptor {
	private final String name;

	/**
	 * Constructor
	 * 
	 * @param name
	 *            the name of the configuration
	 */
	public AbstractConfigurationDeploymentDescriptor(String name) {
		this.name = name;
	}

	/**
	 * Get the name of the configuration to deploy.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

}
