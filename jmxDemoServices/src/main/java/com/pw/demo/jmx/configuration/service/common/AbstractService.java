package com.pw.demo.jmx.configuration.service.common;

/**
 * Abstract base class for a Service.
 * 
 * @param <ServiceInterface>
 *            the main interface this service is be providing
 * @author Przemyslaw Walat
 */
public abstract class AbstractService<ServiceInterface> {

	private final Class<ServiceInterface> serviceInterface;

	/**
	 * Constructor
	 * 
	 * @param serviceInterface
	 *            - the main interface this service will be providing
	 */
	public AbstractService(Class<ServiceInterface> serviceInterface) {
		this.serviceInterface = serviceInterface;
	}

	protected Class<ServiceInterface> getServiceInterface() {
		return serviceInterface;
	}

}