package com.pw.demo.jmx.persistence.dao.message;

import java.util.List;

import com.pw.demo.jmx.persistence.dao.common.DataAccessObject;
import com.pw.demo.jmx.persistence.model.message.entity.Message;
import com.pw.demo.jmx.persistence.model.message.entity.MessageType;
import com.pw.demo.jmx.services.common.PaginationData;

/**
 * Data Access Object for {@link Message}
 * 
 * @author Przemyslaw Walat
 *
 */
public interface MessageDAO extends DataAccessObject<Message> {

	/**
	 * Get all messages, regardless of the filter
	 * 
	 * @param pagingData
	 *            {@link PaginationData} for the request
	 * 
	 * @param readOnly
	 *            specify whether {@link Message} instances returned should be
	 *            read only, if set to true, any changes made to entities
	 *            returned <b>will not</> be reflected in the database
	 * @return the list of all {@link Message}s
	 */
	List<Message> getAll(PaginationData pagingData, Boolean readOnly);

	/**
	 * Returned only those {@link Message} instances, which have
	 * {@link MessageType} not matching the types specified in the filter
	 * 
	 * @param pagingData
	 *            {@link PaginationData} for the request
	 * 
	 * @param filter
	 *            a list of {@link MessageType}s that <b>will not</b> be
	 *            returned by the service
	 * @param readOnly
	 *            specify whether Message instances returned should be read
	 *            only, if set to true, any changes made to entities returned
	 *            <b>will not</> be reflected in the database
	 * @return a list of {@link Message} instances that have {@link MessageType}
	 *         other that those given in the filter
	 */
	List<Message> getFiltered(PaginationData pagingData,
			List<MessageType> filter, Boolean readOnly);

}