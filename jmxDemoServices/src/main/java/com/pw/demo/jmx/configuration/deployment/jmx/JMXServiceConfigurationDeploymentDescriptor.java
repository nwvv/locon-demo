package com.pw.demo.jmx.configuration.deployment.jmx;

/**
 * Builds on top of the {@link JMXConfigurationDeploymentDescriptor} and
 * provides the main interface of the service this descrpitor's configuration is
 * associated with. The service interface will be used to create configuration's
 * name.
 * 
 * @param <MXBeanInterface>
 *            - interface of the MXBean holding the configuration
 * @param <ServiceInterface>
 *            - interface of the service that this descriptor is associated with
 * 
 * @author Przemyslaw Walat
 */
public abstract class JMXServiceConfigurationDeploymentDescriptor<MXBeanInterface, ServiceInterface>
		extends JMXConfigurationDeploymentDescriptor<MXBeanInterface> {

	private final Class<ServiceInterface> serviceInterface;

	/**
	 * Constructor
	 * 
	 * @param mxBeanInterface
	 *            the interface of the MXBean used to store configuration
	 * @param serviceInterface
	 *            the main interface of the service this configuration is
	 *            associated with
	 */
	public JMXServiceConfigurationDeploymentDescriptor(
			Class<MXBeanInterface> mxBeanInterface,
			Class<ServiceInterface> serviceInterface) {
		super(serviceInterface.getSimpleName(), mxBeanInterface);
		this.serviceInterface = serviceInterface;
	}

	/**
	 * Get the main interface of the service this descrptior is associated with
	 * 
	 */
	public Class<ServiceInterface> getServiceInterface() {
		return serviceInterface;
	}

}
