package com.pw.demo.jmx.exception;

/**
 * Application-specific exception, thrown in case an unrecoverable exception
 * occurs.
 * 
 * @author Przemyslaw Walat
 *
 */
public class JMXDemoSystemException extends RuntimeException {

	private static final long serialVersionUID = -6161844780621670340L;

	/**
	 * 
	 * Constructor for JMXDemoSystemException
	 * 
	 * @param message
	 *            - The message describing the error that occured
	 */
	public JMXDemoSystemException(String message) {
		super(message);
	}

	/**
	 * Constructor for JMXDemoSystemException
	 * 
	 * @param message
	 *            - The message describing the error that occured
	 * @param cause
	 *            - The underlying cause
	 */
	public JMXDemoSystemException(String message, Throwable cause) {
		super(message, cause);
	}

}
